package com.flamee.flamee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        btnStart.setOnClickListener(this);
    }

    private void findViews() {
        btnStart = (Button) findViewById(R.id.btn_start);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_start:

                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constants.KEY_TOP_SCORE, 1 );
                startActivityForResult(intent, 111);

                break;

        }
    }
}
